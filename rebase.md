1. sync to the latest commit from remote master

```bash
git pull
```

2. create new branch to write and commit code to this branch

```bash
git checkout -b new_branch
```

3. work on branch, make commits

```bash
git add .
git commit -m 'updated header'
```

4. switch back to local master branch and pull the latest changes

```bash
git checkout master
git pull
```

5. checkout feature branch and rebase against local master

```bash
git checkout new_branch
git rebase master
```

6. Now that feature branch doesn’t have any conflicts, switch back to master branch and place changes to master.

```bash
git checkout master
git rebase new_branch
```

7. push changes up to remote master.

```bash
git push
```

